Citing and Referencing
======================


Reference
---------
If you would like to cite or reference Phytoplankton Photophysiology Utilities, please use:

Source: phyto_photo_utils https://gitlab.com/tryankeogh/phytophotoutils retrieved on 30 May 2019.



Authors and contributors
------------------------

Thomas Ryan-Keogh: Southern Ocean Carbon and Climate Observatory

Charlotte Robinson: Curtin University 