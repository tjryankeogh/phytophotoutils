API Documentation
=================

Loading
-------
.. automodule:: phyto_photo_utils._load
    :members:

General Tools
-------------
.. automodule:: phyto_photo_utils._tools
    :members:


Saturation
----------
.. automodule:: phyto_photo_utils._saturation
    :members:


Relaxation
----------
.. automodule:: phyto_photo_utils._relaxation
    :members:


Spectral Correction
-------------------
.. automodule:: phyto_photo_utils._spectral_correction
    :members:


Fluorescence Light Curves
-------------------------
.. automodule:: phyto_photo_utils._etr
    :members:


Plotting
--------
.. automodule:: phyto_photo_utils._plot
    :members:
